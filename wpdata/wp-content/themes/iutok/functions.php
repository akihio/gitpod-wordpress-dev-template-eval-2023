<?php

/**
 * Load CSS of parent and children themes
 */
function iut_wp_enqueue_scripts() {

    $parenthandle = 'twentynineteen-style';
    $theme        = wp_get_theme();

    // Load parent CSS
    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css', 
        array(),
        $theme->parent()->get( 'Version' )
    );

    // Load child CSS (this theme)
    wp_enqueue_style(
        'iut-style',
        get_stylesheet_uri(), 
        array( $parenthandle ),
        $theme->get( 'Version' )
    );

}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );


/**
 * Register new custom post type "recipe"
 */
function iut_register_post_type_recipe() {

	register_post_type(
		'recipe',
		array(
			'labels'				=> array(
				'name'					=> 'Recettes',
				'singular_name'			=> 'Recette',
			),
			'public'				=> true,
			'publicly_queryable'	=> true,
			'show_in_rest'			=> true,
			'hierarchical'			=> false,
			'supports'				=> array( 'title', 'editor', 'thumbnail' ),
			'has_archive'			=> 'recettes',
			'rewrite'				=> array( 'slug' => 'recette' ),
		)
	);

}

add_action('init', 'iut_register_post_type_recipe', 10) ;


/**
 * Register metabox for custom post type recipe
 */

// Declare metabox
function iut_add_meta_boxes_recipe( $post ) {

	add_meta_box(
		'iut_mbox_recipe',
		'Infos complémentaires',
		'iut_mbox_recipe_content',
		'recipe'
	);

}

add_action( 'add_meta_boxes', 'iut_add_meta_boxes_recipe' );

// Display metabox content
function iut_mbox_recipe_content( $post ) {

	// Get meta value
	$iut_ingredients = get_post_meta(
		$post->ID,
		'iut-ingredients',
		true
	);

	echo '<p>';
	echo '<label for="iut-ingredients">Ingrédients</label>';
	echo '<textarea id="iut-ingredients" name="iut-ingredients" value="' . $iut_ingredients . '"></textarea>';
	echo '</p>';

}

// Save post meta
function iut_save_post( $post_id ) {

	if ( isset( $_POST['iut-ingredients'] ) && !empty( $_POST['iut-ingredients'] ) ) {

		update_post_meta(
			$post_id,
			'iut-ingredients',
			sanitize_textarea_field( $_POST['iut-ingredients'] )
		);
	}

}

add_action( 'save_post', 'iut_save_post' );


/**
 * Bonus : register custom taxonomy "season"
 */
function iut_register_taxonomy_season() {

	register_taxonomy(
		'season',
		'recipe',
		array(
			'labels'				=> array(
				'name'					=> 'Saisons',
				'singular_name'			=> 'Saison',
			),
			'public'				=> true,
			'publicly_queryable'	=> true,
			'show_in_rest'			=> true,
			'hierarchical'			=> false,
			'rewrite'				=> array( 'slug' => 'saison' ),
		)
	);

}

add_action('init', 'iut_register_taxonomy_season', 11) ;